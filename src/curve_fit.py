#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt


def fit_function(Var_Vector, coef_a, coef_b, coef_c):
    ## Note that the function must be given data exactly in this format...
    ## Would it work with a dictionary ??
    ##
    # variables (parameters) of the equation used for fitting the data
    x, y = Var_Vector
    ##
    # equation of the curve, for example: a*(x^2) + b*y + c
    return (coef_a*(x**2) + coef_b*y + coef_c)


# values of each parameter during the experiments / in the observed population
var_x_values = [1, 2, 3, 4, 5, 6]
var_y_values = [4, 5, 6, 7, 8, 9]

# aggregate the variables into one array (or Vector) for the fitting process
var_array = numpy.array([var_x_values, var_y_values])


# output values obtained from the experiments or observations
output_data = [5.11, 3.9, 5.3, 12, 11.3, 16.4]


# initial guess for the coeficients
initial_guess = numpy.array([1, 1, 1])


fitParams, fitCovariances = curve_fit(fit_function, (var_array), output_data, initial_guess)

print("fit coefficients:\n", fitParams)
