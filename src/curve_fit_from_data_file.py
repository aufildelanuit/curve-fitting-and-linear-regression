#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy
from scipy.optimize import curve_fit
import pandas
import matplotlib.pyplot as plt


## could also be written: def fit_function(Var_Vector, coef_a, coef_b, coef_c):
## but this shorthand avoids changing the function definition every time we change the equation
def fit_function(Var_Vector, *Coef_Vector):
	## Note that the function must be given data exactly in this format...
	## Would it work with a dictionary ??
	##
	###########################################################################
	# variables (parameters) of the equation used for fitting the data
	x, y = Var_Vector
	##
	# coefficients to be optimized (the numbers we are trying to calculate)
	coef_a, coef_b, coef_c = Coef_Vector
	##
	# equation of the curve, for example: a*(x^2) + b*y + c
	return (coef_a*(x**2) + coef_b*y + coef_c)
	###########################################################################


# initial guess for the coeficients
# it is REALLY important that this array has exactly as many entries as we have parameters to optimize in the equation
# (in the given example: a, b, c)
###############################################################################
initial_guess = {
	"a": 1,
	"b": 1,
	"c": 1,
} # the order is important
###############################################################################
initial_guess_array=numpy.array(list(initial_guess.values()))


# values of each parameter during the experiments / in the observed population
# here, we read from a comma-separated file "data.csv", using the pandas library
###############################################################################
# /!\ in the data file, each entry was separated with both a comma "," and *spaces* (no tab)
data = pandas.read_csv("data.csv", skipinitialspace=True)
###############################################################################


# make a table of all variables from the data, one variable per line
# does not include the last field (last column in the data file), which is supposed to be the result, and not a variable
var_array = data[data.columns[:-1]].values.T


# output values obtained from the experiments or observations
# here, the output (result) of each experiment / observation is kept in the data.csv file
# the result column is named "result" in the file.
output_data = data[data.columns[-1]].values


fitParams, fitCovariances = curve_fit(fit_function, (var_array), output_data, initial_guess_array)

print("\nfit coefficients:")
print(*fitParams)
print("R^2 :", numpy.corrcoef(fit_function(var_array, *fitParams), data["result"])[0,1]**2 )
