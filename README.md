# Python curve-fitting and linear regression project

This project contains a few python scripts for different levels of data analysis:

- __curve_fit.py__ performs a simple curve fitting on a rather small amount of data "hard-coded" in the script itself.

- __curve_fit_from_data_file.py__ does the same thing, but reads its data from a file named data.csv

- __better_curve_fit_from_data_file.py__ uses another library (lmfit) to obtain more detailed results, etc.

- __stepwise_linear_fit_from_data_file.py__ is a fully automated script that performs linear regression and a stepwise variable selection.


This project is fully equiped with all the requirements to be built and run using docker (or alternatives like podman).

See the detailed instructions in the __INSTRUCTIONS.txt__ file.

*New* : From now on, the main program can also be accessed on Google Colab, at the [following address](https://colab.research.google.com/drive/16VdxXpLFiAddsyRXABxkCyR_t79PtJkb?usp=sharing).

The Jupyter notebook can also be downloaded from this repository.
